<?php
/**
 * Created by PhpStorm.
 * User: guillaume
 * Date: 05/07/18
 * Time: 07:50
 */

namespace App\Command;

use App\Entity\CallTicket;
use App\Serializer\Converter\CallTicketCSVConverter;
use Doctrine\Common\Annotations\AnnotationReader;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Helper\ProgressBar;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

use Symfony\Component\Serializer\Encoder\CsvEncoder;
use Symfony\Component\Serializer\Mapping\Factory\ClassMetadataFactory;
use Symfony\Component\Serializer\Mapping\Loader\AnnotationLoader;
use Symfony\Component\Serializer\Normalizer\ArrayDenormalizer;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Serializer;

class ImportCommand extends Command
{
    private $em;

    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
        parent::__construct();
    }

    protected function configure()
    {
        $this
            ->setName('app:import')
            ->setDescription('Import CSV')
            ->setHelp('This command allows you to import csv file...')
            ->addArgument('fileName', InputArgument::REQUIRED, 'Which file csv do import?')
        ;

    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $output->writeln('Import CSV started ...');
        $output->writeln('Configure import ...');
        //Configure sérialzer
        $nameConverter = new CallTicketCSVConverter();
        $normalizer = new ObjectNormalizer(new ClassMetadataFactory(new AnnotationLoader(new AnnotationReader())), $nameConverter);
        $encoder = new CsvEncoder(';', '"', '\\', '*', true);
        $serializer = new Serializer([$normalizer, new ArrayDenormalizer()], [$encoder]);

        //Get DATAS csv to import and its headers
        $output->writeln('Get data for import from file ...');
        $data = file_get_contents($input->getArgument('fileName'));
        $lines = explode("\n", $data);
        $data = implode("\n", array_slice($lines, 2));

        //Create entities from csv file
        $output->writeln('Create objects from data...');
        $entities = $serializer->deserialize(utf8_encode($data), CallTicket::class.'[]','csv', array(
            'allow_extra_attributes' => true,
        ));

        //Prepare progress bar CLI
        $output->writeln('Prepare objects to persist in database...');
        $section = $output->section();
        $progress = new ProgressBar($section);
        $progress->start(count($entities));

        //Persist entities
        foreach($entities as $index => $entity) {
            try {
                $this->em->persist($entity);
            } catch(\Exception $e) {
                $output->writeln('Line : ' . $index, $e->getMessage());
            } finally {
                $progress->advance(1);
            }
        }

        //Flush the entities manager
        $output->writeln('Please wait during writing in database...');
        try {
            $this->em->flush();
        } catch(\Exception $e) {
            $output->writeln($e->getMessage());
        }
        $output->writeln('Successfull for writing in database...');

    }
}