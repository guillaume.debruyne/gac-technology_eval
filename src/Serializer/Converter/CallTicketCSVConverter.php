<?php
/**
 * Created by PhpStorm.
 * User: guillaume
 * Date: 08/07/18
 * Time: 16:21
 */

namespace App\Serializer\Converter;


use Symfony\Component\Serializer\NameConverter\NameConverterInterface;

class CallTicketCSVConverter implements NameConverterInterface
{
    const MAPPING = [
        'N° abonné	' => 'subscriber',
        'Date ' => 'startedDateAt',
        'Heure'=> 'startedTimeAt',
        'Durée/volume réel' => 'realVolume',
        'Durée/volume facturé' => 'billedVolume',
        'Type ' => 'type',
        'Compte facturé' => 'ignoringAttribute',
        'N° Facture' => 'ignoringAttribute',
    ];

    const DEMAPPING = [
         'subscriber'=> 'N° abonné	',
         'startedDateAt' => 'Date ',
         'startedTimeAt'=>  'Heure',
         'realVolume' => 'Durée/volume réel',
         'billedVolume' => 'Durée/volume facturé',
         'type' => 'Type ',
    ];

    public function normalize($propertyName)
    {
        return DEMMAPING[$propertyName];
        return array_flip(self::MAPPING)[$propertyName];
    }

    public function denormalize($propertyName)
    {
        return self::MAPPING[$propertyName];
    }
}