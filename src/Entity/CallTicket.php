<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;

/**
 * @ORM\Entity(repositoryClass="App\Repository\CallTicketRepository")
 */
class CallTicket
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $subscriber;

    /**
     * @ORM\Column(type="date", nullable=true)
     * @Assert\DateTime()
     */
    private $startedDateAt;

    /**
     * @ORM\Column(type="time", nullable=true)
     * @Assert\DateTime()
     */
    private $startedTimeAt;

    /**
     * @ORM\Column(type="decimal", precision=10, scale=2)
     */
    private $realVolume;

    /**
     * @ORM\Column(type="decimal", precision=10, scale=2)
     */
    private $billedVolume;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $type;

    public function __construct()
    {
        $this->startedDateAt = new \DateTime();
        $this->startedTimeAt = new \DateTime();
    }

    public function getId()
    {
        return $this->id;
    }

    public function getSubscriber(): ?string
    {
        return $this->subscriber;
    }

    public function setSubscriber(string $subscriber): self
    {
        $this->subscriber = $subscriber;

        return $this;
    }

    public function getStartedDateAt(): ?\DateTime
    {
        return $this->startedDateAt;
    }

    public function setStartedDateAt(string $startedDateAt): self
    {
        if (isset($startedDateAt) && !empty($startedDateAt)) {
            $date = \DateTime::createFromFormat('d/m/Y', $startedDateAt);
            if ($date instanceof \DateTime) $this->startedDateAt = $date;
        }

        return $this;
    }

    public function getStartedTimeAt(): ?\DateTime
    {
        return $this->startedTimeAt;
    }

    public function setStartedTimeAt(string $startedTimeAt): self
    {
        if (isset($startedTimeAt) && !empty($startedTimeAt)) {
            $time = \DateTime::createFromFormat('H:i:s', $startedTimeAt);
            if ($time instanceof \DateTime) $this->startedTimeAt = $time;
        }

        return $this;
    }

    public function getRealVolume()
    {
        return $this->realVolume;
    }

    public function setRealVolume($realVolume): self
    {

        $this->realVolume = $this->convertVolumeToDecimal($realVolume);

        return $this;
    }

    public function getBilledVolume()
    {
        return $this->billedVolume;
    }

    public function setBilledVolume($billedVolume): self
    {
        $this->billedVolume = $this->convertVolumeToDecimal($billedVolume);

        return $this;
    }

    public function getType(): ?string
    {
        return $this->type;
    }

    public function setType(string $type): self
    {
        $this->type = $type;

        return $this;
    }

    private function convertVolumeToDecimal(string $volume): string
    {
        if(empty($volume)) return 0;
        $tab = explode( ':', $volume);
        switch(count($tab)) {
            case 1:
                return $tab[0];
                break;
            case 2:
                return round($tab[0]*60 + $tab[1], 2);
                break;
            case 3:
                return round($tab[0]*3600 + $tab[1]*60 + $tab[2], 2);
                break;
        }
    }
}
